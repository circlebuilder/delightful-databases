# delightful databases [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/master/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A curated list of delightful databases in all shapes and sizes, and all FOSS.

<!-- Everyone is invited to contribute. To do so, please read guidelines at: https://codeberg.org/teaserbot-labs/delightful -->

## Table of contents

- [Relational databases](#relational-databases)
- [Key-value databases](#key-value-databases)
- [Document-oriented databases](#document-oriented-databases)
- [Column databases](#column-databases)
- [Graph databases](#graph-databases)
- [Time series databases](#time-series-databases)
- [Datalog databases](#datalog-databases)
- [Multi-modal databases](#multi-modal-databases)
- [Special purpose](#special-purpose)
- [Related delight](#related-delight)
- [Maintainers](#maintainers)
- [Contributors](#contributors)
- [License](#license)

## Relational databases

| Website | Summary | License |
| :---: | --- | --- |
| [Dolt](https://github.com/dolthub/dolt) | A SQL database that you can fork, clone, branch, merge, push and pull just like a git repository. | Apache License 2.0 |
| [DuckDB](https://github.com/cwida/duckdb) | An embeddable SQL OLAP Database Management System. | MIT |
| [H2](https://github.com/h2database/h2database) | An embeddable RDBMS written in Java. | MPL 2.0 / EPL 1.0 |
| [MariaDB](https://github.com/MariaDB/server) | Community developed fork of MySQL server. | GPLv2 |
| [MonetDBe](https://github.com/MonetDBSolutions/MonetDBe-Python/) | Embedded MonetDB with a Python frontend and fast Numpy/Pandas support. | MPL-2.0 |
| [OrioleDB](https://github.com/orioledb/orioledb) | A modern cloud-native storage engine (... and solving some PostgreSQL wicked problems) | MIT |
| [PostgreSQL](https://git.postgresql.org/gitweb/?p=postgresql.git) | Powerful, open source object-relational database system. | PostgreSQL Licence |
| [rqlite](https://github.com/rqlite/rqlite) | The lightweight, distributed relational database built on SQLite. | MIT |
| [SQLite](https://sqlite.org/src) | Small, fast, self-contained, high-reliability, full-featured, SQL database engine. | Public domain |
| [TiDB](https://github.com/pingcap/tidb) | An open source distributed HTAP database compatible with the MySQL protocol. | Apache License 2.0 |
| [ts-sql](https://github.com/codemix/ts-sql) | A SQL database implemented purely in TypeScript type annotations. | MIT |
| [YugabyteDB](https://github.com/yugabyte/yugabyte-db) | A high-performance, cloud-native distributed SQL database that aims to support all PostgreSQL features. | Apache License 2.0 |

## Key-value databases

| Website | Summary | License |
| :---: | --- | --- |
| [AntidoteDB](https://github.com/AntidoteDB/antidote) | A planet scale, highly available, transactional database built on CRDT technology. | Apache License 2.0 |
| [BadgerDB](https://github.com/dgraph-io/badger) | An embeddable, persistent and fast key-value (KV) database written in pure Go. | Apache License 2.0 |
| [bbold](https://github.com/etcd-io/bbolt) | An embedded key/value database for Go. | MIT |
| [BonsaiDB](https://github.com/khonsulabs/bonsaidb) | A developer-friendly document database that grows with you, written in Rust. | Apache License 2.0 |
| [BuntDB](https://github.com/tidwall/buntdb) | An embeddable, in-memory key/value database for Go with custom indexing and geospatial support. | MIT |
| [CubDB](https://github.com/lucaong/cubdb) | An embedded key-value database for the Elixir language. It is designed for robustness, and for minimal need of resources. | Apache License 2.0 |
| [Goleveldb](https://github.com/syndtr/goleveldb) | LevelDB key/value database in Go. | BSD-2-Clause |
| [Graviton](https://github.com/deroproject/graviton) | A simple, fast, versioned, authenticated, embeddable key-value store database in pure Golang. | GPLv3 |
| [ImmuDB](https://github.com/codenotary/immudb) | A lightweight, high-speed immutable database for systems and applications. | Apache License 2.0 |
| [KeyDB](https://github.com/JohnSully/KeyDB) | Multithreaded Fork of Redis. | BSD 3-Clause |
| [LevelDB](https://github.com/google/leveldb) | A fast key-value storage library written at Google that provides an ordered mapping from string keys to string values. | BSD 3-Clause |
| [Pebble](https://github.com/cockroachdb/pebble) | RocksDB/LevelDB inspired key-value database in Go. | BSD 3-Clause |
| [QDBM](https://dbmx.net/qdbm/) | A high-performance implementation of the dbm database engine. | LGPL-2.1 |
| [Quadrable](https://github.com/hoytech/quadrable) | Authenticated multi-version database: sparse binary merkle tree with compact partial-tree proofs. | BSD 2-Clause |
| [Redis](https://github.com/antirez/redis) | Open source, in-memory data structure store, used as a database, cache and message broker. | BSD 3-Clause |
| [RocksDB](https://github.com/facebook/rocksdb) | A library that provides an embeddable, persistent key-value store for fast storage. | GPLv2 |
| [TerarkDB](https://github.com/bytedance/terarkdb) | A RocksDB compatible KV storage engine with better performance. | Apache License 2.0 |
| [Tidis](https://github.com/yongman/tidis) | Distributed transactional NoSQL database, Redis protocol compatible using tikv as backend. | MIT |
| [Urkel](https://github.com/chjj/liburkel) | An optimized and cryptographically provable key-value store (i.e. an urkel tree). | MIT |

## Document-oriented databases

| Website | Summary | License |
| :---: | --- | --- |
| [CondensationDB](https://github.com/CondensationDB/Condensation) | A zero-trust distributed database that ensures data ownership and data security. | Apache License 2.0 |
| [CouchDB](https://github.com/apache/couchdb) | Open source NoSQL document database that stores data in JSON documents accessible via HTTP. | Apache License 2.0  |
| [Earthstar](https://github.com/cinnamon-bun/earthstar) | Offline-first, distributed, syncable, embedded document database for use in p2p software. | AGPL 3.0  |
| [FerretDB](https://github.com/FerretDB/FerretDB) | A truly Open Source MongoDB alternative. | Apache License 2.0 |
| [Lowdb](https://github.com/typicode/lowdb) | A small local JSON database powered by Lodash (supports Node, Electron and the browser). | MIT |
| [NeDB](https://github.com/louischatriot/nedb) | The JavaScript Database for Node.js, nw.js, electron and the browser. | MIT |
| [noms](https://github.com/attic-labs/noms) | The versioned, forkable, syncable, decentralized database philosophically descendant from the Git version control system. | Apache License 2.0 |
| [PouchDB](https://github.com/pouchdb/pouchdb) | An open-source JavaScript database inspired by Apache CouchDB that is designed to run well within the browser. | Apache License 2.0 |
| [RefineDB](https://github.com/losfair/RefineDB) | A strongly-typed document database that runs on any transactional key-value store. | MIT |
| [RethinkDB](https://github.com/rethinkdb/rethinkdb) | Distributed, highly available, open-source database that stores schemaless JSON documents. | Apache License 2.0  |
| [RxDB](https://github.com/pubkey/rxdb) | A realtime Database for JavaScript Applications. | Apache License 2.0 |
| [SirDB](https://github.com/c9fe/sirdb) | A git diffable JSON database on yer filesystem. | AGPL-3.0 |
| [SirixDB](https://github.com/sirixdb/sirix) | An Evolutionary, Accumulate-Only Database System. | BSD-3 Clause |
| [SleekDB](https://github.com/rakibtg/SleekDB) | Pure PHP NoSQL database with no dependency. Flat file, JSON based document database. | MIT |
| [TiKV](https://github.com/tikv/tikv) | Distributed transactional key-value database. | Apache License 2.0 |

## Column databases

| Website | Summary | License |
| :---: | --- | --- |
| [Cassandra](https://gitbox.apache.org/repos/asf?p=cassandra.git) | Free and open-source, distributed, wide column store, NoSQL database management system. | Apache License 2.0 |
| [FrostDB](https://github.com/polarsignals/frostdb) | Embeddable column database written in Go. | Apache License 2.0 |
| [Scylla](https://github.com/scylladb/scylla) | NoSQL data store using the seastar framework, compatible with Apache Cassandra. | AGPL-3.0 |

## Graph databases

| Website | Summary | License |
| :---: | --- | --- |
| [Blazegraph](https://github.com/blazegraph/database) | High-performance graph database supporting Blueprints and RDF/SPARQL APIs. | GPLv2 |
| [BrightstartDB](https://github.com/BrightstarDB/BrightstarDB) | A native .NET RDF triple store that uses LINQ for querying. | MIT |
| [Cayley](https://github.com/cayleygraph/cayley) | Open source database for Linked Data. It is inspired by the graph database behind Google's Knowledge Graph (formerly Freebase). | Apache License 2.0 |
| [Dgraph](https://github.com/dgraph-io/dgraph) | Fast, transactional, distributed Graph Database with support for GraphQL-like query syntax. | Apache License 2.0 |
| [Fortune.js](https://github.com/fortunejs/fortune) | Non-native graph database abstraction layer for Node.js and web browsers. | MIT |
| [Gaffer](https://github.com/gchq/Gaffer) | A large-scale entity and relation database supporting aggregation of properties. | Apache License 2.0 |
| [Gun](https://github.com/amark/gun) | A realtime, decentralized, offline-first, graph protocol to sync the web. | Apache License 2.0 |
| [JanusGraph](https://github.com/JanusGraph/janusgraph) | A highly scalable graph database optimized for storing and querying large graphs with billions of vertices and edges. | Apache License 2.0 / CC-BY-4.0 |
| [NebulaGraph](https://github.com/vesoft-inc/nebula) | A distributed, fast open-source graph database featuring horizontal scalability and high availability. | Apache License 2.0 |
| [Neo4j](https://github.com/neo4j/neo4j) | Highly scalable native graph database, purpose-built to leverage not only data but also data relationships. | GPLv3 |
| [RecallGraph](https://github.com/RecallGraph/RecallGraph) | A versioning data store for time-variant graph data. | Apache License 2.0 |
| [TerminusDB](https://github.com/terminusdb/terminusdb-server) | A model driven in-memory graph database designed for the web-age using JSON-LD exchange format. | GPLv3 |
| [TypeDB](https://github.com/vaticle/typedb) | A strongly-typed database with a rich and logical type system. | AGPL-3.0 |

## Time series databases

| Website | Summary | License |
| :---: | --- | --- |
| [InfluxDB](https://github.com/influxdata/influxdb) | Scalable datastore for metrics, events, and real-time analytics. | MIT |
| [QuestDB](https://github.com/questdb/questdb) | An open source SQL database designed to process time series data, faster. | Apache License 2.0 |
| [TimescaleDB](https://github.com/timescale/timescaledb) | Open-source database built for analyzing time-series data with the power and convenience of SQL. | Apache License 2.0 |

## Datalog databases

| Website | Summary | License |
| :---: | --- | --- |
| [Datahike](https://github.com/replikativ/datahike) | A durable datalog implementation, powered by an efficient Datalog query engine and adaptable for distribution. | EPL-1.0 |
| [Datascript](https://github.com/tonsky/DataScript) | Immutable database and Datalog query engine for Clojure, ClojureScript and JS. | EPL-1.0 |
| [Datalevin](https://github.com/juji-io/datalevin) | Simple, fast and durable Datalog database for everyone. Available for Clojure on JVM and GraalVM. | EPL-1.0 |
| [Crux](https://github.com/juxt/crux) | General purpose bitemporal database for SQL, Datalog & graph queries. | MIT |

## Multi-modal databases

| Website | Summary | License |
| :---: | --- | --- |
| [ArangoDB](https://github.com/arangodb/arangodb) | A native multi-model database with flexible data models for documents, graphs, and key-values. | Apache License 2.0 |
| [BerkeleyDB](http://www.oracle.com/database/berkeley-db/) | An embeddable database allowing developers the choice of SQL, Key/Value, XML/XQuery or Java Object storage | AGPL-3.0 |
| [EdgeDB](https://github.com/edgedb/edgedb) | A graph-relational database that takes the best parts of relational databases, graph databases, and ORMs. | Apache 2.0 License |
| [FoundationDB](https://github.com/apple/foundationdb/) | A distributed database designed to handle large volumes of structured data across clusters of commodity servers. | Apache License 2.0 |
| [go-orbit-db](https://github.com/berty/go-orbit-db) | Go version of OrbitDB P2P Database on IPFS | Apache License 2.0 |
| [Irmin](https://github.com/mirage/irmin) | A distributed database that follows the same design principles as Git. | ISC License |
| [LedisDB](https://github.com/ledisdb/ledisdb) | A high performance NoSQL Database Server powered by Go. | MIT |
| [OrbitDB](https://github.com/orbitdb/orbit-db) | A serverless, distributed, peer-to-peer database based on IPFS | MIT |
| [OrientDB](https://github.com/orientechnologies/orientdb) | The most versatile DBMS supporting Graph, Document, Reactive, Full-Text, Geospatial and Key-Value models. | Apache License 2.0 |
| [MartenDB](https://github.com/JasperFx/Marten) | A .NET transactional Document DB and Event Store on top of PostgreSQL. | MIT |

## Special purpose

| Website | Summary | License |
| :---: | --- | --- |
| [eyros](https://github.com/peermaps/eyros) | A multi-dimensional interval database designed for peer-to-peer distribution and good for geospatial and time-series data. | Apache License 2.0 |
| [IceFireDB](https://github.com/IceFireDB/IceFireDB) | Distributed disk storage database based on Raft and Redis protocol. | Apache License 2.0 |
| [Prometheus](https://github.com/prometheus/prometheus) | A monitoring system and time series database. | Apache License 2.0 |
| [SpiceDB](https://github.com/authzed/spicedb) | A Zanzibar-inspired database that stores, computes, and validates application permissions. | Apache License 2.0 |
| [Vaxine](https://github.com/vaxine-io/vaxine) | A rich-CRDT database that solves the global write-path latency problem for backend application. | Apache License 2.0 |
| [Xbase](https://sourceforge.net/projects/xdb/) | A collection of tools for manipulating Xbase type datafiles and indices. | LGPL-2.1 |

## Related delight

- [dbdb.io](https://dbdb.io) - A database of databases

## Maintainers

If you have questions or feedback regarding this list, then please create
an [Issue](https://codeberg.org/yarmo/delightful-databases/issues) in our tracker, and optionally `@mention` one or more of our maintainers:

- [`@yarmo`](https://codeberg.org/yarmo)

## Contributors

With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](https://codeberg.org/teaserbot-labs/delightful/src/branch/master/delight-us.md#attribution-of-contributors) if you are missing).

## License

[![CC0 Public domain. This work is free of known copyright restrictions.](https://i.creativecommons.org/p/mark/1.0/88x31.png)](https://creativecommons.org/publicdomain/zero/1.0/)
